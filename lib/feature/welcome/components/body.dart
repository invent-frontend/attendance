import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:invent_login/widget//rounded_button.dart';
import 'package:invent_login/constants/color.dart';
import 'package:invent_login/feature/login/screen/login_screen.dart';
import 'package:invent_login/feature/signup/signup_screen.dart';
import 'package:get/get.dart';

import 'background.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "WELCOME TO EDU",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: context.height * 0.05),
            SvgPicture.asset(
              "assets/icons/chat.svg",
              height: context.height * 0.45,
            ),
            SizedBox(height: context.height * 0.05),
            RoundedButton(
              text: "LOGIN",
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                       return LoginScreen();
                     // return null;
                    },
                  ),
                );
              },
            ),
            RoundedButton(
              text: "SIGN UP",
              color: kPrimaryLightColor,
              textColor: Colors.black,
              press: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context){

                      return SignUpScreen();
                   // return null;
                    },
                ),
                );
              },
            ),
          ],

        ),

      ),
    );
  }


}