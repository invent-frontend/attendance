import 'package:equatable/equatable.dart';
import 'package:invent_login/entity/report_response.dart';

abstract class ReportState extends Equatable {
  const ReportState();

  @override
  List<Object?> get props => [];
}

class ReportLoading extends ReportState {}

class ReportFailed extends ReportState {
  final Object reason;

  const ReportFailed({required this.reason});
}

class ReportFetched extends ReportState {
  final ReportResponse response;

  const ReportFetched({required this.response});

  @override
  List<Object?> get props => [response];
}

class ReportUnInitialize extends ReportState {}

class ReportItemRemoved extends ReportState {}

class RemoveReportItemFailed extends ReportState {
  final Object reason;

  const RemoveReportItemFailed({required this.reason});
}