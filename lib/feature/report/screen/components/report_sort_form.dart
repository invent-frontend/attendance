import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:invent_login/entity/report_request.dart';
import 'package:invent_login/feature/login/bloc/login_bloc.dart';
import 'package:invent_login/feature/login/bloc/login_state.dart';
import 'package:invent_login/feature/report/report_bloc.dart';
import 'package:invent_login/feature/report/report_event.dart';
import 'package:invent_login/feature/report/report_state.dart';

import '../../../../widget/spinner_dropdown.dart';

class ReportSortForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    var currentMonth = DateTime.now().month;
    var yearOptions = createYearsDropDown();
    var yearOptionsNotifier = ValueNotifier(yearOptions.last);
    var monthOptions = createMonthsDropDown();
    var monthOptionsNotifier = ValueNotifier(monthOptions[currentMonth - 1]);
    return _ReportSortFormState(
      yearOptions: yearOptions,
      yearOptionsNotifier: yearOptionsNotifier,
      monthOptions: monthOptions,
      monthOptionsNotifier: monthOptionsNotifier,
    );
  }

  List<SpinnerDropDownItem> createYearsDropDown() {
    int currentYear = DateTime.now().year;
    List<SpinnerDropDownItem> options = [];
    List.of([-2, -1, 0]).forEach((element) {
      var strOpt = (currentYear + element).toString();
      options.add(SpinnerDropDownItem(id: strOpt, name: strOpt));
    });
    return options;
  }

  List<SpinnerDropDownItem> createMonthsDropDown() {
    return List.of([
      SpinnerDropDownItem(id: '1', name: 'Januari'),
      SpinnerDropDownItem(id: '2', name: 'Februari'),
      SpinnerDropDownItem(id: '3', name: 'Maret'),
      SpinnerDropDownItem(id: '4', name: 'April'),
      SpinnerDropDownItem(id: '5', name: 'Mei'),
      SpinnerDropDownItem(id: '6', name: 'Juni'),
      SpinnerDropDownItem(id: '7', name: 'Juli'),
      SpinnerDropDownItem(id: '8', name: 'Agustus'),
      SpinnerDropDownItem(id: '9', name: 'September'),
      SpinnerDropDownItem(id: '10', name: 'Oktober'),
      SpinnerDropDownItem(id: '11', name: 'November'),
      SpinnerDropDownItem(id: '12', name: 'Desember'),
    ]);
  }
}

class _ReportSortFormState extends State<ReportSortForm> {
  final List<SpinnerDropDownItem> yearOptions;
  final List<SpinnerDropDownItem> monthOptions;
  final ValueNotifier yearOptionsNotifier;
  final ValueNotifier monthOptionsNotifier;

  _ReportSortFormState({
    required this.yearOptions,
    required this.monthOptions,
    required this.yearOptionsNotifier,
    required this.monthOptionsNotifier,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(builder: (context, loginState) {
      if (loginState is LoginSuccess) {
        return BlocBuilder<ReportBloc, ReportState>(builder: (context, reportState) {
          var provider = context.read<ReportBloc>();
          if (reportState is ReportItemRemoved || reportState is ReportUnInitialize) {
            fetchReport(provider, loginState);
          }
          var self = this;
          monthOptionsNotifier.addListener(() {
            self.fetchReport(provider, loginState);
          });
          yearOptionsNotifier.addListener(() {
            self.fetchReport(provider, loginState);
          });
          // fetchReport(provider, loginState);
          return Container(
            width: MediaQuery.of(context).size.width / 2,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: DropDown(
                        list: monthOptions, valueNotifier: monthOptionsNotifier),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: DropDown(
                        list: yearOptions, valueNotifier: yearOptionsNotifier),
                  ),
                ),
              ],
            ),
          );
        });
      }
      return Center();
    });
  }

  void fetchReport(ReportBloc provider, LoginSuccess state) {
    provider.add(FetchReport(
        request: ReportRequest(
            year: yearOptionsNotifier.value.id,
            month: monthOptionsNotifier.value.id),
        employeeId: state.response.data!.first.employeeId));
  }
}
