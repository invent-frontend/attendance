import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:invent_login/feature/report/report_bloc.dart';
import 'package:invent_login/feature/report/report_event.dart';

import '../../report_state.dart';

class ReportView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ReportViewState();
  }
}

class _ReportViewState extends State<ReportView> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReportBloc, ReportState>(builder: (context, state) {
      var provider = context.read<ReportBloc>();
      if (state is ReportFetched) {
        var data = state.response.data;
        return Expanded(
            child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width / 2,
          child: ListView.builder(
              shrinkWrap: true,
              itemCount: data.length,
              itemBuilder: (buildContext, index) {
                return Card(
                  child: Row(
                    children: [
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Container(
                                alignment: Alignment.centerLeft,
                                child: Text("Tanggal Absen",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.start),
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                    data[index].createdDate.split(" ").first,
                                    textAlign: TextAlign.start),
                              )
                            ],
                            mainAxisSize: MainAxisSize.max,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Container(
                                alignment: Alignment.centerLeft,
                                child: Text("Tipe Absen",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.start),
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                child: Text(data[index].typeAbsen,
                                    textAlign: TextAlign.start),
                              )
                            ],
                            mainAxisSize: MainAxisSize.max,
                          ),
                        ),
                      ),
                      IconButton(
                        onPressed: () => {
                          showDialog(
                              context: context,
                              builder: (buildContext) {
                                return AlertDialog(
                                  title: Text(
                                      "Apakah Anda yakin hendak menghapus data absen ini (${data[index].createdDate.split(" ").first})?"),
                                  content: Text(
                                      "Dampak dari tindakan Anda tidak dapat dikembalikan."),
                                  actions: [
                                    TextButton(
                                        onPressed: () =>
                                            {Navigator.of(context).pop()},
                                        child: Text("Batal")),
                                    TextButton(
                                        onPressed: () => {
                                              Navigator.of(context).pop(),
                                          provider.add(RemoveReportItem(reportItem: data[index]))
                                            },
                                        child: Text(
                                          "Ya, Hapus",
                                          style: TextStyle(color: Colors.red),
                                        )),
                                  ],
                                );
                              })
                        },
                        icon: Icon(
                          Icons.delete,
                          color: Colors.red,
                        ),
                      ),
                    ],
                  ),
                );
              }),
        ));
      }
      if (state is ReportLoading) {
        return CircularProgressIndicator();
      }
      return Center();
    });
  }
}
