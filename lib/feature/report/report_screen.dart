import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart';
import 'package:invent_login/feature/report/report_bloc.dart';
import 'package:invent_login/feature/report/repositories/report_repository.dart';
import 'package:invent_login/feature/report/screen/components/report_sort_form.dart';
import 'package:invent_login/feature/report/screen/components/report_view.dart';
import 'package:invent_login/network/api_client.dart';

import 'report_state.dart';

class ReportScreen extends StatelessWidget {
  const ReportScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          ReportBloc(ReportRepository(ApiClient(httpClient: Client()))),
      child: BlocListener<ReportBloc, ReportState>(
        listener: (context, state) {},
        child: Material(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ReportSortForm(),
              ReportView(),
            ],
          ),
        ),
      ),
    );
  }
}
