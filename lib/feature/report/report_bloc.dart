import 'dart:developer';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:invent_login/entity/remove_report_item_response.dart';
import 'package:invent_login/entity/report_response.dart';
import 'package:invent_login/feature/report/report_event.dart';
import 'package:invent_login/feature/report/report_state.dart';
import 'package:invent_login/feature/report/repositories/report_repository.dart';

class ReportBloc extends Bloc<ReportEvent, ReportState> {
  final ReportRepository _repository;
  
  ReportBloc(this._repository): super(ReportUnInitialize()) {
    on<FetchReport>(
        (event, emit) async {
          emit(ReportLoading());
          try {
            final ReportResponse response = await _repository.fetchReport(event.request, event.employeeId);
            log("success " + response.status);
            emit(ReportFetched(response: response));
          } catch(error) {
            log("error " + error.runtimeType.toString());
            if (error is TypeError) {
              throw error;
            }
            emit(ReportFailed(reason: error));
          }
        }
    );
    on<RemoveReportItem>(
        (event, emit) async {
          try {
            final RemoveReportItemResponse response = await _repository.removeReportItem(event.reportItem);
            log("success " + response.status);
            emit(ReportItemRemoved());
          } catch(error) {
            log("error " + error.runtimeType.toString());
            if (error is TypeError) {
              throw error;
            }
            emit(RemoveReportItemFailed(reason: error));
          }
        }
    );
  }

}