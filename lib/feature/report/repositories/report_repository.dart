import 'package:invent_login/entity/report_item.dart';
import 'package:invent_login/entity/report_request.dart';
import 'package:invent_login/entity/report_response.dart';
import 'package:invent_login/network/api_client.dart';

import '../../../entity/remove_report_item_response.dart';

class ReportRepository {

  final ApiClient _apiClient;

  ReportRepository(this._apiClient);

  Future<ReportResponse> fetchReport(ReportRequest request, employeeId) async {
    return await _apiClient.fetchReport(request, employeeId);
  }

  Future<RemoveReportItemResponse> removeReportItem(ReportItem reportItem) async {
    return await _apiClient.removeReportItem(reportItem);
  }

}