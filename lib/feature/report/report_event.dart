import 'package:equatable/equatable.dart';
import 'package:invent_login/entity/report_item.dart';
import 'package:invent_login/entity/report_request.dart';

abstract class ReportEvent extends Equatable {
  const ReportEvent();

  @override
  List<Object?> get props => [];
}

class FetchReport extends ReportEvent {
  final ReportRequest request;
  final String employeeId;

  const FetchReport({required this.request, required this.employeeId});
}

class RemoveReportItem extends ReportEvent {
  final ReportItem reportItem;

  const RemoveReportItem({required this.reportItem});
}
