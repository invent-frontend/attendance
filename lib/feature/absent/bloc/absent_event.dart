import 'package:equatable/equatable.dart';
import 'package:invent_login/entity/post_present_request.dart';

abstract class AbsentEvent extends Equatable {
  const AbsentEvent();
}

class Absent extends AbsentEvent {
  final PostPresentRequest request;

  const Absent({required this.request});

  @override
  List<Object?> get props => [];
}