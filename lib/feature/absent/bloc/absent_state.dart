import 'package:equatable/equatable.dart';
import 'package:invent_login/entity/post_present_response.dart';

abstract class AbsentState extends Equatable {
  const AbsentState();

  @override
  List<Object?> get props => [];
}

class AbsentLoading extends AbsentState {}

class AbsentFailed extends AbsentState {}

class AbsentSuccess extends AbsentState {
  final PostPresentResponse response;

  const AbsentSuccess({required this.response});

  @override
  List<Object?> get props => [response];
}

class AbsentUnInitialize extends AbsentState {}