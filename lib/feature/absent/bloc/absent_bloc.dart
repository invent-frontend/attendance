import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:invent_login/entity/post_present_response.dart';
import 'package:invent_login/feature/absent/bloc/absent_event.dart';
import 'package:invent_login/feature/absent/repositories/absent_repository.dart';
import 'package:invent_login/feature/absent/bloc/absent_state.dart';

class AbsentBloc extends Bloc<AbsentEvent, AbsentState> {
  final AbsentRepository _repository;

  AbsentBloc(this._repository): super(AbsentUnInitialize()) {
    on<Absent>(
        (event, emit) async {
          emit(AbsentLoading());
          try {
            final PostPresentResponse response = await _repository.postPresent(event.request);
            emit(AbsentSuccess(response: response));
          } catch(_) {
            emit(AbsentFailed());
            emit(AbsentUnInitialize());
          }
        }
    );
  }
}