import 'package:invent_login/entity/post_present_request.dart';
import 'package:invent_login/entity/post_present_response.dart';
import 'package:invent_login/network/api_client.dart';

class AbsentRepository {

  final ApiClient _apiClient;

  AbsentRepository(this._apiClient);

  Future<PostPresentResponse> postPresent(PostPresentRequest request) async {
    return await _apiClient.postPresent(request);
  }

}