import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart';
import 'package:invent_login/feature/absent/bloc/absent_bloc.dart';
import 'package:invent_login/feature/absent/bloc/absent_state.dart';
import 'package:invent_login/feature/absent/repositories/absent_repository.dart';
import 'package:invent_login/feature/login/screen/components/background.dart';
import 'package:invent_login/network/api_client.dart';

import 'components/body.dart';

class AbsentScreen extends StatelessWidget {
  const AbsentScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AbsentBloc(
        AbsentRepository(ApiClient(httpClient: Client()))
      ),
      child: BlocListener<AbsentBloc, AbsentState>(
        listener: (context, state) {
          if (state is AbsentFailed) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("Gagal absen"),
            ));
          }
          if (state is AbsentSuccess) {
            Navigator.pop(context);
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("Berhasil absen"),
            ));
          }
        },
        child: Material(
          child: Background(
            child: Body(),
          ),
        ),
      ),
    );
  }

}
