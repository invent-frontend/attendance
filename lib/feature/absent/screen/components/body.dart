import 'package:flutter/material.dart';
import 'package:invent_login/feature/absent/screen/components/absent_form.dart';

class Body extends StatelessWidget{
  const Body({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: AbsentForm(),
    );
  }
}