import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:invent_login/entity/post_present_request.dart';
import 'package:invent_login/feature/absent/bloc/absent_bloc.dart';
import 'package:invent_login/feature/absent/bloc/absent_event.dart';
import 'package:invent_login/feature/absent/bloc/absent_state.dart';
import 'package:invent_login/feature/login/bloc/login_bloc.dart';
import 'package:invent_login/feature/login/bloc/login_state.dart';
import 'package:invent_login/widget/rounded_button.dart';
import 'package:invent_login/widget/rounded_input_field.dart';
import 'package:invent_login/widget/spinner_dropdown.dart';
import 'package:provider/src/provider.dart';

class AbsentForm extends StatefulWidget {
  const AbsentForm({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _AbsentFormState();
  }
}

class _AbsentFormState extends State<AbsentForm> {

  final noteInputController = TextEditingController();

  @override
  void dispose() {
    noteInputController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var options = List.of([
      SpinnerDropDownItem(id: "WFH", name: "WFH"),
      SpinnerDropDownItem(id: "Kantor", name: "Kantor"),
      SpinnerDropDownItem(id: "Meeting", name: "Meeting"),
      SpinnerDropDownItem(id: "Sakit", name: "Sakit"),
      SpinnerDropDownItem(id: "Ijin", name: "Ijin"),
      SpinnerDropDownItem(id: "Cuti", name: "Cuti"),
    ]);
    ValueNotifier absentOption = ValueNotifier(options.first);

    var provider = context.read<AbsentBloc>();
    return BlocBuilder<LoginBloc, LoginState>(
      builder: (context, state) {
        if (state is LoginSuccess) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: Get.height * 0.03),
              SvgPicture.asset(
                "assets/icons/login.svg",
                height: Get.height * 0.35,
              ),
              SizedBox(height: Get.height * 0.03),
              DropDown(
                list: options,
                valueNotifier: absentOption,
              ),
              RoundedInputField(
                hintText: "Catatan",
                icon: Icons.note,
                controller: noteInputController,
              ),
              BlocBuilder<AbsentBloc, AbsentState>(
                builder: (context, absentState) {
                  if (absentState is AbsentLoading) {
                    return const CircularProgressIndicator();
                  }
                  return RoundedButton(
                    text: "ABSEN MASUK",
                    press: () {
                      provider.add(Absent(request: PostPresentRequest(
                          employeeId: state.response.data!.first.employeeId,
                          note: noteInputController.text,
                          typePresent: absentOption.value.id
                      )));
                    },
                  );
                }
              ),
              SizedBox(height: Get.height * 0.03),
            ],
          );
        }
        return Center();
      },
    );
  }
}