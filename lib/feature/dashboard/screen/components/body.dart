import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:invent_login/feature/login/bloc/login_bloc.dart';
import 'package:invent_login/feature/login/bloc/login_state.dart';
import 'package:invent_login/feature/login/screen/components/background.dart';
import 'package:provider/src/provider.dart';

import 'grid_dashboard.dart';

class Body extends StatelessWidget{

  const Body({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var state = context.read<LoginBloc>().state;
    if (state is LoginSuccess) {
      return Background(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 40,
            ),
            Padding(
              padding: EdgeInsets.only(left: 30, right: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        state.response.data![0].employeeName,
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 18),

                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Text(
                        "Welcome",
                        style: TextStyle(
                            color: Color(0xffa29aac),
                            fontWeight: FontWeight.w600,
                            fontSize: 14),

                      ),
                    ],
                  ),

                  SvgPicture.asset(
                    "assets/icons/signup.svg",
                    height: Get.height * 0.18,
                  ),

                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            GridDashboard()
          ],
        ),


      );
    }
    return Center();
  }


}