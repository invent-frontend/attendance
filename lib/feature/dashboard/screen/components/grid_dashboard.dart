import 'package:flutter/material.dart';
import 'package:invent_login/constants/routes.dart';
import 'package:invent_login/utils/helpers/responsive.dart';
import 'package:sizer/sizer.dart';

class GridDashboard extends StatelessWidget {
  Items item1 = new Items(
    title: "Absensi",
    img: "assets/icons/calendar.png",
    routes: Routes.ABSENT,
  );

  Items item2 = new Items(
    title: "Daily Task",
    img: "assets/icons/todo.png",
  );
  Items item3 = new Items(
    title: "Report Absensi",
    img: "assets/icons/todo.png",
    routes: Routes.REPORT,
  );
  Items item4 = new Items(
    title: "Pengajuan Cuti",
    img: "assets/icons/todo.png",
  );
  Items item5 = new Items(
    title: "To do",
    img: "assets/icons/todo.png",
  );
  Items item6 = new Items(
    title: "Settings",
    img: "assets/icons/setting.png",
  );

  @override
  Widget build(BuildContext context) {
    List<Items> myList = [item1, item2, item3, item4, item5, item6];
    return Flexible(
      child: GridView.count(
          childAspectRatio: 1.0,
          padding: EdgeInsets.only(left: 30, right: 30),
          crossAxisCount: Responsive().getResponsiveValue(
            context: context,
            forLargeScreen: 6,
            forMediumScreen: 4,
            forTabletScreen: 3,
            forMobLandScapeMode: 4,
            forSmallScreen: 2,
            forAllDefault: 4,
          ),
          crossAxisSpacing: 20,
          mainAxisSpacing: 20,
          children: myList.map((data) {
            return GestureDetector(
              onTap: () {
                //todo route
                if (data.routes != null) {
                  Navigator.pushNamed(context, data.routes!);
                }
              },
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.bottomLeft,
                      end: Alignment.topRight,
                      colors: [
                        Color.fromRGBO(250, 189, 190, 1.0),
                        Color.fromRGBO(178, 213, 241, 1),
                      ]),
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      data.img,
                      width: 42,
                    ),
                    SizedBox(
                      height: 14,
                    ),
                    Text(
                      data.title,
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                        fontSize: Responsive().getResponsiveValue(
                          context: context,
                          forAllDefault: 8.sp,
                          forLargeScreen: 4.sp,
                          forSmallScreen: 10.sp
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                  ],
                ),
              ),
            );
          }).toList()),
    );
  }
}

class Items {
  String title;
  String img;
  String? routes;

  Items({required this.title, required this.img, this.routes});
}
