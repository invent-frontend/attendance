import 'package:invent_login/entity/login_request.dart';
import 'package:invent_login/entity/login_response.dart';
import 'package:invent_login/network/api_client.dart';

class LoginRepository {

  final ApiClient apiClient;

  LoginRepository({required this.apiClient});

  Future<LoginResponse> login(LoginRequest request) async {
    return await apiClient.login(request);
  }

}