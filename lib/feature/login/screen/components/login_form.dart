import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:invent_login/entity/login_request.dart';
import 'package:invent_login/feature/login/bloc/login_bloc.dart';
import 'package:invent_login/feature/login/bloc/login_event.dart';
import 'package:invent_login/feature/login/bloc/login_state.dart';
import 'package:invent_login/utils/helpers/responsive.dart';
import 'package:invent_login/widget/rounded_button.dart';
import 'package:invent_login/widget/rounded_input_field.dart';
import 'package:invent_login/widget/rounded_password_field.dart';
import 'package:get/get.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _LoginFormState();
  }
}

class _LoginFormState extends State<LoginForm> {

  final employeeIdInputController = TextEditingController();
  final passwordInputController = TextEditingController();

  @override
  void dispose() {
    employeeIdInputController.dispose();
    passwordInputController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var provider = context.read<LoginBloc>();

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SizedBox(height: context.height * 0.03),
        SvgPicture.asset(
          "assets/icons/login.svg",
          height: context.height * Responsive().getResponsiveValue(
            context: context,
            forAllDefault: 0.25,
            forLargeScreen: 0.35,
          ),
        ),
        SizedBox(height: context.height * 0.03),
        RoundedInputField(
          hintText: "Employee ID",
          controller: employeeIdInputController,
        ),
        RoundedPasswordField(
          controller: passwordInputController,
        ),
        BlocBuilder<LoginBloc, LoginState>(
            builder: (context, state) {
              if (state is LoginUnInitialize) {
                return RoundedButton(
                  text: "LOGIN",
                  press: () {
                    provider.add(Login(request: LoginRequest(
                        employeeId: employeeIdInputController.text,
                        password: passwordInputController.text
                    )));
                  },
                );
              }
              return const CircularProgressIndicator();
            }
        ),
        SizedBox(height: context.height * 0.03),
        /* AlreadyHaveAnAccountCheck(
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return SignUpScreen();
                      //return null;
                    },
                  ),
                );
              },
            ),*/
      ],
    );
  }

}