import 'package:flutter/material.dart';
import 'package:invent_login/feature/login/screen/components/login_form.dart';

import 'background.dart';

class Body extends StatelessWidget{
  const Body({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Background(
      child: SingleChildScrollView(
        child: LoginForm()
      ),
    );
  }


}