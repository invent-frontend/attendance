import 'package:equatable/equatable.dart';
import 'package:invent_login/entity/login_request.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
}

class Login extends LoginEvent {

  final LoginRequest request;

  const Login({required this.request});

  @override
  List<Object?> get props => [];
}
