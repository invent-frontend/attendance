import 'package:bloc/bloc.dart';
import 'package:invent_login/entity/login_response.dart';
import 'package:invent_login/feature/login/bloc/login_event.dart';
import 'package:invent_login/feature/login/repositories/login_repository.dart';
import 'package:invent_login/feature/login/bloc/login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final LoginRepository repository;

  LoginBloc({required this.repository}) : super(LoginUnInitialize()) {
    on<Login>(
        (event, emit) async {
          emit(LoginLoading());
          try {
            final LoginResponse response = await repository.login(event.request);
            emit(LoginSuccess(response: response));
          } catch(_) {
            emit(LoginFailed());
            emit(LoginUnInitialize());
          }
        }
    );
  }
}
