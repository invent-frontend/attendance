import 'package:equatable/equatable.dart';
import 'package:invent_login/entity/login_response.dart';

abstract class LoginState extends Equatable {
  const LoginState();

  @override
  List<Object?> get props => [];
}

class LoginLoading extends LoginState {}

class LoginFailed extends LoginState{}

class LoginSuccess extends LoginState {
  final LoginResponse response;

  const LoginSuccess({required this.response});

  @override
  List<Object?> get props => [response];
}

class LoginUnInitialize extends LoginState {}