import 'package:flutter/material.dart';
import 'package:invent_login/feature/Signup/components/background.dart';
import 'package:invent_login/feature/Signup/components/or_divider.dart';
import 'package:invent_login/feature/Signup/components/social_icon.dart';
import 'package:flutter_svg/svg.dart';
import 'package:invent_login/feature/login/screen/login_screen.dart';
import 'package:invent_login/widget/already_have_an_account_acheck.dart';
import 'package:invent_login/widget/rounded_button.dart';
import 'package:invent_login/widget/rounded_input_field.dart';
import 'package:invent_login/widget/rounded_password_field.dart';
import 'package:get/get.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "SIGNUP",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: context.height * 0.03),
            SvgPicture.asset(
              "assets/icons/signup.svg",
              height: context.height * 0.35,
            ),
            RoundedInputField(
              hintText: "Your Email",
            ),
            RoundedPasswordField(
            ),
            RoundedButton(
              text: "SIGNUP",
              press: () {},
            ),
            SizedBox(height: context.height * 0.03),
            AlreadyHaveAnAccountCheck(
              login: false,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return LoginScreen();
                    },
                  ),
                );
              },
            ),
            OrDivider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SocalIcon(
                  iconSrc: "assets/icons/facebook.svg",
                  press: () {},
                ),
                SocalIcon(
                  iconSrc: "assets/icons/twitter.svg",
                  press: () {},
                ),
                SocalIcon(
                  iconSrc: "assets/icons/google-plus.svg",
                  press: () {},
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
