import 'package:flutter/material.dart';
import 'package:invent_login/constants/color.dart';
import 'package:invent_login/widget/text_field_container.dart';

class RoundedPasswordField extends StatelessWidget{

  final TextEditingController? controller;

  const RoundedPasswordField({
    Key? key,
    this.controller,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return TextFieldContainer(
      child: TextField(
        obscureText: true,
        controller: controller,
        cursorColor: kPrimaryColor,
        decoration: InputDecoration(
          hintText: "Password",
          icon: Icon(
            Icons.lock,
            color: kPrimaryColor,
          ),
          suffixIcon: Icon(
            Icons.visibility,
            color: kPrimaryColor,
          ),
          border: InputBorder.none
        ),
      ),

    );
  }


}