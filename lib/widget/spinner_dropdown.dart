import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:invent_login/widget/text_field_container.dart';

class DropDown extends StatefulWidget {
  final List<SpinnerDropDownItem?> list;

  final ValueNotifier valueNotifier;

  DropDown({Key? key, required this.list, required this.valueNotifier}) : super(key: key);

  @override
  _DropDownItem createState() => _DropDownItem();
}

class _DropDownItem extends State<DropDown> {
  SpinnerDropDownItem? selected;

  @override
  void initState() {
    selected = widget.valueNotifier.value;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: DropdownButton<SpinnerDropDownItem>(
        value: selected,
        isExpanded: true,
        icon: Icon(Icons.arrow_drop_down),
        iconSize: 24,
        elevation: 16,
        underline: Container(
          height: 2,
          color: Colors.transparent,
        ),
        onChanged: (SpinnerDropDownItem? data) {
          setState(() {
            this.selected = data;
          });
          widget.valueNotifier.value = data;
        },
        items: widget.list.map<DropdownMenuItem<SpinnerDropDownItem>>((SpinnerDropDownItem? value) {
          return DropdownMenuItem<SpinnerDropDownItem>(
            value: value,
            child: Text(value?.name ?? '-'),
          );
        }).toList(),
      ),
    );
  }
}

class SpinnerDropDownItem extends Equatable {
  final String id;
  final String name;

  SpinnerDropDownItem({required this.id, required this.name});

  @override
  List<Object?> get props => [id, name];
}
