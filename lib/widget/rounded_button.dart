import 'package:flutter/material.dart';
import 'package:invent_login/constants/color.dart';
import 'package:invent_login/utils/helpers/responsive.dart';
import 'package:get/get.dart';

class RoundedButton extends StatefulWidget {

  final String text;
  final Function press;
  final Color color , textColor;

  const RoundedButton({
    Key? key,
    required this.text,
    required this.press,
    this.color = kPrimaryColor,
    this.textColor = Colors.white,}) : super(key: key);

  @override
  _RoundedButtonState createState() => _RoundedButtonState();
}

class _RoundedButtonState extends State<RoundedButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      width: Responsive().getResponsiveValue(
        context: context,
        forAllDefault: context.width / 2 * 0.8,
        forSmallScreen: context.width * 0.5,
        forTabletScreen: context.width * 0.8,
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(29),
        child: newElevatedButton(),
      ),

    );
  }

  Widget newElevatedButton(){
    return ElevatedButton(
      child: Text(
        widget.text,
        style: TextStyle(color: widget.textColor),
      ),
      onPressed: () {
        widget.press();
      },
      style: ElevatedButton.styleFrom(
        primary: widget.color,
        padding: const EdgeInsets.symmetric(horizontal: 40,vertical: 20),
        textStyle: TextStyle(
            color: widget.textColor, fontSize: 14,fontWeight: FontWeight.w500
        ),
      ),
    );
  }

}