import 'package:flutter/material.dart';
import 'package:invent_login/constants/color.dart';
import 'package:invent_login/utils/helpers/responsive.dart';
import 'package:get/get.dart';

class TextFieldContainer extends StatelessWidget {
  final Widget child;

  const TextFieldContainer({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: Responsive().getResponsiveValue(
        context: context,
        forAllDefault: context.width / 2 * 0.8,
        forSmallScreen: context.width * 0.7,
        forTabletScreen: context.width * 0.8,
      ),
      decoration: BoxDecoration(
        color: kPrimaryLightColor,
        borderRadius: BorderRadius.circular(29),
      ),
      child: child,
    );
  }
}
