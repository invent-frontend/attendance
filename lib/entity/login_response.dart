import 'package:json_annotation/json_annotation.dart';

part 'login_response.g.dart';

@JsonSerializable()
class LoginResponse {
  @JsonKey(name: 'STATUS')
  String status;

  @JsonKey(name: 'DATA')
  List<LoginData>? data;

  LoginResponse({required this.status, this.data});

  static LoginResponse fromJson(Map<String, dynamic> json) =>
      _$LoginResponseFromJson(json);

  Map<String, dynamic> toJson() => _$LoginResponseToJson(this);
}

@JsonSerializable()
class LoginData {
  @JsonKey(name: 'employee_id')
  String employeeId;

  @JsonKey(name: 'employee_name')
  String employeeName;

  LoginData({required this.employeeId, required this.employeeName});

  static LoginData fromJson(Map<String, dynamic> json) =>
      _$LoginDataFromJson(json);

  Map<String, dynamic> toJson() => _$LoginDataToJson(this);
}
