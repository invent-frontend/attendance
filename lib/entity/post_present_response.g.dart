// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_present_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostPresentResponse _$PostPresentResponseFromJson(Map<String, dynamic> json) =>
    PostPresentResponse(
      status: json['STATUS'] as String,
      lastId: json['LAST_ID'] as int,
      data: PostPresentRequest.fromJson(json['DATA'] as Map<String, dynamic>),
      message: json['MESSAGE'] as String,
    );

Map<String, dynamic> _$PostPresentResponseToJson(
        PostPresentResponse instance) =>
    <String, dynamic>{
      'STATUS': instance.status,
      'LAST_ID': instance.lastId,
      'DATA': instance.data,
      'MESSAGE': instance.message,
    };
