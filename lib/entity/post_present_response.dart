import 'package:invent_login/entity/post_present_request.dart';
import 'package:json_annotation/json_annotation.dart';

part 'post_present_response.g.dart';

@JsonSerializable()
class PostPresentResponse {
  @JsonKey(name: 'STATUS')
  String status;

  @JsonKey(name: 'LAST_ID')
  int lastId;

  @JsonKey(name: 'DATA')
  PostPresentRequest data;

  @JsonKey(name: 'MESSAGE')
  String message;

  PostPresentResponse(
      {required this.status,
      required this.lastId,
      required this.data,
      required this.message});

  static PostPresentResponse fromJson(Map<String, dynamic> json) =>
      _$PostPresentResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PostPresentResponseToJson(this);
}
