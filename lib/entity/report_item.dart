import 'package:json_annotation/json_annotation.dart';

part 'report_item.g.dart';

@JsonSerializable()
class ReportItem {

  @JsonKey(name: 'id')
  String id;

  @JsonKey(name: 'employee_id')
  String employeeId;

  @JsonKey(name: 'employee_name')
  String employeeName;

  @JsonKey(name: 'type_absen')
  String typeAbsen;

  @JsonKey(name: 'note')
  String note;

  @JsonKey(name: 'created_date')
  String createdDate;

  ReportItem({
    required this.id,
    required this.employeeId,
    required this.employeeName,
    required this.typeAbsen,
    required this.note,
    required this.createdDate
  });

  static ReportItem fromJson(Map<String, dynamic> json) =>
      _$ReportItemFromJson(json);

  Map<String, dynamic> toJson() => _$ReportItemToJson(this);

}