import 'package:invent_login/entity/report_item.dart';
import 'package:json_annotation/json_annotation.dart';

part 'report_response.g.dart';

@JsonSerializable()
class ReportResponse {
  @JsonKey(name: 'STATUS')
  String status;

  @JsonKey(name: 'DATA')
  List<ReportItem> data;

  ReportResponse({
    required this.status,
    required this.data
  });

  static ReportResponse fromJson(Map<String, dynamic> json) =>
      _$ReportResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ReportResponseToJson(this);

}