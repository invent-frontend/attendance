import 'package:json_annotation/json_annotation.dart';

part 'post_present_request.g.dart';

@JsonSerializable()
class PostPresentRequest {
  @JsonKey(name: 'employee_id')
  String employeeId;

  @JsonKey(name: 'type_absen')
  String typePresent;

  @JsonKey(name: 'note')
  String note;

  PostPresentRequest(
      {required this.employeeId,
      required this.typePresent,
      required this.note});

  static PostPresentRequest fromJson(Map<String, dynamic> json) =>
      _$PostPresentRequestFromJson(json);

  Map<String, dynamic> toJson() => _$PostPresentRequestToJson(this);
}
