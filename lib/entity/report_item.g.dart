// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'report_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReportItem _$ReportItemFromJson(Map<String, dynamic> json) => ReportItem(
      id: json['id'] as String,
      employeeId: json['employee_id'] as String,
      employeeName: json['employee_name'] as String,
      typeAbsen: json['type_absen'] as String,
      note: json['note'] as String,
      createdDate: json['created_date'] as String,
    );

Map<String, dynamic> _$ReportItemToJson(ReportItem instance) =>
    <String, dynamic>{
      'id': instance.id,
      'employee_id': instance.employeeId,
      'employee_name': instance.employeeName,
      'type_absen': instance.typeAbsen,
      'note': instance.note,
      'created_date': instance.createdDate,
    };
