// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_present_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostPresentRequest _$PostPresentRequestFromJson(Map<String, dynamic> json) =>
    PostPresentRequest(
      employeeId: json['employee_id'] as String,
      typePresent: json['type_absen'] as String,
      note: json['note'] as String,
    );

Map<String, dynamic> _$PostPresentRequestToJson(PostPresentRequest instance) =>
    <String, dynamic>{
      'employee_id': instance.employeeId,
      'type_absen': instance.typePresent,
      'note': instance.note,
    };
