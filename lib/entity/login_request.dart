import 'package:json_annotation/json_annotation.dart';

part 'login_request.g.dart';

@JsonSerializable()
class LoginRequest {
  @JsonKey(name: 'employee_id')
  String employeeId;

  @JsonKey(name: 'password')
  String password;

  LoginRequest({required this.employeeId, required this.password});

  static LoginRequest fromJson(Map<String, dynamic> json) =>
      _$LoginRequestFromJson(json);

  Map<String, dynamic> toJson() => _$LoginRequestToJson(this);
}
