// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'remove_report_item_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RemoveReportItemResponse _$RemoveReportItemResponseFromJson(
        Map<String, dynamic> json) =>
    RemoveReportItemResponse(
      status: json['STATUS'] as String,
      message: json['MESSAGE'] as int,
    );

Map<String, dynamic> _$RemoveReportItemResponseToJson(
        RemoveReportItemResponse instance) =>
    <String, dynamic>{
      'STATUS': instance.status,
      'MESSAGE': instance.message,
    };
