import 'package:json_annotation/json_annotation.dart';

part 'report_request.g.dart';

@JsonSerializable()
class ReportRequest {
  @JsonKey(name: 'year_sort')
  String year;

  @JsonKey(name: 'month_sort')
  String month;

  ReportRequest({
    required this.year,
    required this.month
  });

  static ReportRequest fromJson(Map<String, dynamic> json) =>
      _$ReportRequestFromJson(json);

  Map<String, dynamic> toJson() => _$ReportRequestToJson(this);

}