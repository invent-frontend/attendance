import 'package:json_annotation/json_annotation.dart';

part 'remove_report_item_response.g.dart';

@JsonSerializable()
class RemoveReportItemResponse {
  @JsonKey(name: 'STATUS')
  String status;

  @JsonKey(name: 'MESSAGE')
  int message;

  RemoveReportItemResponse(
      {required this.status,
        required this.message});

  static RemoveReportItemResponse fromJson(Map<String, dynamic> json) =>
      _$RemoveReportItemResponseFromJson(json);

  Map<String, dynamic> toJson() => _$RemoveReportItemResponseToJson(this);
}
