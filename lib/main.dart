import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart';
import 'package:invent_login/constants/routes.dart';
import 'package:invent_login/feature/absent/screen/absent_screen.dart';
import 'package:invent_login/feature/login/bloc/login_bloc.dart';
import 'package:invent_login/feature/login/repositories/login_repository.dart';
import 'package:invent_login/feature/login/screen/login_screen.dart';
import 'package:invent_login/feature/report/report_screen.dart';
import 'package:invent_login/network/api_client.dart';
import 'package:sizer/sizer.dart';

import 'constants/color.dart';

void main() {
  runApp( MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return BlocProvider(
        create: (context) => LoginBloc(
            repository:
            LoginRepository(apiClient: ApiClient(httpClient: Client()))),
        child: MaterialApp(
          title: 'Absen Invent',
          theme: ThemeData(
            primaryColor: kPrimaryColor,
            scaffoldBackgroundColor: Colors.white,
          ),
          initialRoute: Routes.HOME,
          routes: {
            Routes.HOME : (context) => LoginScreen(),
            Routes.ABSENT : (context) => AbsentScreen(),
            Routes.REPORT: (context) => ReportScreen(),
          },
        ),
      );
    });
  }
}
