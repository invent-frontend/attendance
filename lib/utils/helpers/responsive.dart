import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class Responsive {
  // function reponsible for providing value according to screensize
  getResponsiveValue(
      {dynamic forAllDefault,
      dynamic forSmallScreen,
      dynamic forMediumScreen,
      dynamic forLargeScreen,
      dynamic forMobLandScapeMode,
      dynamic forTabletScreen,
      required BuildContext context}) {
    if (isLargeScreen(context)) {
      return forLargeScreen ?? forAllDefault;
    } else if (isMediumScreen(context)) {
      return forMediumScreen ?? forAllDefault;
    } else if (isTabletScreen(context)) {
      return forTabletScreen ?? forMediumScreen ?? forAllDefault;
    } else if (isSmallScreen(context)) {
      return forSmallScreen ?? forAllDefault;
    } else if (isSmallScreen(context)) {
      return forMobLandScapeMode ?? forAllDefault;
    } else {
      return forAllDefault;
    }
  }

  static bool isLargeScreen(BuildContext context) {
    return Get.width > 1200;
  }

  static bool isSmallScreen(BuildContext context) {
    return Get.width < 800;
  }

  static bool isMediumScreen(BuildContext context) {
    return Get.width > 800 && Get.width < 1200;
  }

  static bool isTabletScreen(BuildContext context) {
    return context.width > 450 && context.width < 800;
  }
}
