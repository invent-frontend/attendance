import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart';
import 'package:invent_login/entity/login_request.dart';
import 'package:invent_login/entity/login_response.dart';
import 'package:invent_login/entity/post_present_request.dart';
import 'package:invent_login/entity/post_present_response.dart';
import 'package:invent_login/entity/remove_report_item_response.dart';
import 'package:invent_login/entity/report_item.dart';
import 'package:invent_login/entity/report_request.dart';
import 'package:invent_login/entity/report_response.dart';
import 'package:invent_login/feature/report/report_event.dart';

class ApiClient {
  final _baseUrl = 'invent-integrasi.co.id';

  final Client httpClient;

  ApiClient({required this.httpClient});

  Future<LoginResponse> login(LoginRequest request) async {
    final response = await _request('lib/api/login_employee', request.toJson());
    return LoginResponse.fromJson(response);
  }

  Future<PostPresentResponse> postPresent(PostPresentRequest request) async {
    final response =
        await _request('lib/api/insert_update_absen', request.toJson());
    return PostPresentResponse.fromJson(response);
  }

  Future<ReportResponse> fetchReport(ReportRequest request, String employeeId) async {
    final response = await _request('lib/api/list_absen/' + employeeId, request.toJson());
    return ReportResponse.fromJson(response);
  }

  Future<RemoveReportItemResponse> removeReportItem(ReportItem reportItem) async {
    final response = await this.httpClient.get(Uri.https(_baseUrl, 'lib/api/delete_absen/${reportItem.id}/'));
    print(response.body);
    return RemoveReportItemResponse.fromJson(jsonDecode(response.body));
  }

  Future<Map<String, dynamic>> _request(
      String path, Map<String, dynamic> request) async {
    final response =
        await this.httpClient.post(Uri.https(_baseUrl, path), body: jsonEncode(request));
    log("$path success : ${response.body}");
    print(response.body);
    if (response.statusCode != 200) {
      log("$path failed : ${response.statusCode} ${response.body}");
      throw new Exception('Failed to call API request $path');
    }
    return jsonDecode(response.body);
  }
}
